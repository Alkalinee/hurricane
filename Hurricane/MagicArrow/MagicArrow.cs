﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Hurricane.MagicArrow
{
    internal class MagicArrow : IDisposable
    {
        public event EventHandler MoveOut;
        public event EventHandler MoveIn;

        public Window BaseWindow { get; set; }

        public MagicArrow(Window window)
        {
            BaseWindow = window;
            Application.Current.Deactivated += Application_Deactivated;
        }

        void Application_Deactivated(object sender, EventArgs e)
        {
            
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool _disposed;
        public virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                Debug.Print("MagicArrow already disposed!");
                return;
            }
            if (disposing)
            {
                Application.Current.Deactivated -= Application_Deactivated;
                // free managed resources
            }
            // free native resources if there are any.
            _disposed = true;
        }

        ~MagicArrow()
        {
            Dispose(false);
        }
    }

    public enum Side { Left, Right }
}