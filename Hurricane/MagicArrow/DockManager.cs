﻿using System;
using System.Runtime.Remoting.Messaging;
using System.Windows;
using System.Windows.Input;
using Hurricane.Model.Skin;
using Hurricane.Utilities;
using Hurricane.Views.Docking;

namespace Hurricane.MagicArrow
{
    class DockManager
    {
        private readonly Window _basewindow;
        private WindowPositionSide? _newSide;
        private readonly MouseHook _mouseHook;
        private bool _isAtRightOrLeftBorder;
        private bool _isAtTop;
        private DockPlaceholderWindow _placeholderWindow;

        public event EventHandler Undocked;
        public event EventHandler Docked;
        public event EventHandler DragStopped;

        public DockingSide CurrentSide { get; set; } //the applied side
        public bool IsDragging { get; set; }

        protected void OnUndocked()
        {
            if (Undocked != null) Undocked(this, EventArgs.Empty);
        }

        protected void OnDocked()
        {
            if (Docked != null) Docked(this, EventArgs.Empty);
        }

        protected void OnDragStopped()
        {
            if (DragStopped != null) DragStopped(this, EventArgs.Empty);
        }

        public DockManager(Window window, DockingSide dockingSide)
        {
            _basewindow = window;
            CurrentSide = dockingSide;
            _mouseHook = new MouseHook();
            _mouseHook.MouseMove += MouseHookOnMouseMove;
        }

        public void DragStart()
        {
            if (IsDragging) return;
            IsDragging = true;
            _newSide = null;
            _mouseHook.Enable();

        }

        private void MouseHookOnMouseMove(object sender, MouseMoveEventArgs e)
        {
            if (!IsDragging || Mouse.LeftButton == MouseButtonState.Released)
                return;

            if (MouseIsLeftRightOrTop(e.X, e.Y, out _newSide))
            {
                var screen = WpfScreen.GetScreenFrom(new Point(e.X, e.Y));
                if (_newSide == WindowPositionSide.Left || _newSide == WindowPositionSide.Right)
                {
                    if (!_isAtRightOrLeftBorder)
                    {
                        _isAtRightOrLeftBorder = true;
                        OpenWindow(NewSide.Value, screen);
                        if (_isAtTop) _isAtTop = false;
                    }
                    return;
                }

                if (_isAtRightOrLeftBorder)
                {
                    _isAtRightOrLeftBorder = false;
                    CloseWindowIfExists();
                }

                if (_newSide == WindowPositionSide.Top)
                {
                    if (!_isAtTop || screen.DeviceName != DisplayingScreen)
                    {
                        _isAtTop = true;
                        OpenWindow(NewSide.Value, screen);
                        DisplayingScreen = screen.DeviceName;
                    }
                }
            }
        }

        private bool MouseIsLeftRightOrTop(int mouseX, int mouseY, out WindowPositionSide? side)
        {
            if (mouseX < WpfScreen.MostLeftX + 5)
            {
                side = WindowPositionSide.Left;
                return true;
            }
            if (mouseX >= WpfScreen.MostRightX - 5)
            {
                side = WindowPositionSide.Right;
                return true;
            }
            if (mouseY < 5)
            {
                side = WindowPositionSide.Top;
                return true;
            }
            side = WindowPositionSide.None;
            return false;
        }

        private bool WindowIsLeftOrRight()
        {
            return _basewindow.Left == WpfScreen.MostLeftX || (_basewindow.Left == WpfScreen.MostRightX - _basewindow.Width);
        }

        private void CloseWindow()
        {
            if (_placeholderWindow != null && _placeholderWindow.IsLoaded)
            {
                _placeholderWindow.Close();
                _placeholderWindow = null;
            }
        }

        private void OpenWindow(WindowPositionSide side, WpfScreen screen)
        {
            if (side == WindowPositionSide.None) return;
            CloseWindow();

            double dockwindowLeft, dockwindowWidth;
            switch (side)
            {
                case WindowPositionSide.Left:
                    dockwindowLeft = WpfScreen.MostLeftX;
                    dockwindowWidth = 300;
                    break;
                case WindowPositionSide.Right:
                    dockwindowLeft = WpfScreen.MostRightX - 300;
                    dockwindowWidth = 300;
                    break;
                case WindowPositionSide.Top:
                    return;
                default:
                    throw new ArgumentOutOfRangeException("side");
            }

            _placeholderWindow = new DockPlaceholderWindow(screen.WorkingArea.Top, dockwindowLeft, screen.WorkingArea.Height, dockwindowWidth);
            _placeholderWindow.Show();
        }
    }

    public enum WindowPositionSide { Left, Right, Top, None }
}
