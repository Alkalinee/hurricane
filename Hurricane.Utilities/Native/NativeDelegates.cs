﻿using System;

namespace Hurricane.Utilities.Native
{
    public static class NativeDelegates
    {
        public delegate IntPtr HookProc(int code, IntPtr wParam, IntPtr lParam);
    }
}
